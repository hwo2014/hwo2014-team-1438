using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using HelloWorldProj;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;

public class Bot {

    private StreamWriter writer;
    List<CarPosition> carPositions;
    GameData gameData;
    string MyCarName { get; set; }
    int PreviousPieceIndex { get; set; }
    float PreviousPieceDistance { get; set; }
    DateTime PreviousTimeStamp { get; set; }
    float PreviousSpeed { get; set; }
    float PreviousThrottleValue { get; set; }
    CarPosition myCar = null;
    List<float> previousSpeeds = new List<float>(){0,0,0};

    public bool SlowDown { get; set; }
    public bool SpeedUp { get; set; }
    public float MaxSpeed { get; set; }
    float throttleValue = 1;
    static string LogFilePath = System.Reflection.Assembly.GetExecutingAssembly().Location + ".txt";
    static bool statsTitleSet = false;
    static string LogFilePath_forStats = System.Reflection.Assembly.GetExecutingAssembly().Location + ".Stats.xls";

    static string Map_finland = "keimola";
    static string Map_germany = "germany";
    static string Map_usa = "usa";
    static string Map_france = "france";

    static bool enableLogging = true;

    static bool isInnerLaneSwitchDone = false;

    public static int CurrentGameTick = 0;
    static float CurveCriticalAngle = 20;

    static Dictionary<float, float> deAccelarationValues = new Dictionary<float, float>();//holds high velocity before throttle was set to zero and the deaccelaration

    TurboInfo turboInfo = null;

	public static void Main(string[] args) {

        string host = "testserver.helloworldopen.com";
        int port = 8091;
        string botName = "Kronos";
        string botKey = "Vad2br0DCEQvEg";
        string mapName = string.Empty;
        bool CI = false;
        
        if (args.Length > 0)
        {
            host = args[0];
            port = int.Parse(args[1]);
            botName = args[2];
            botKey = args[3];

            CI = true;

            if (args.Length > 4)
            {
                mapName = args[4];

                LogFilePath = System.Reflection.Assembly.GetExecutingAssembly().Location + "-" + mapName + ".txt";
                LogFilePath_forStats = System.Reflection.Assembly.GetExecutingAssembly().Location + "-" + mapName + ".Stats.xls";

                CI = false;
                
            }
            else
            {
                enableLogging = false;
            }
        }

        
        if (enableLogging && File.Exists(LogFilePath))
        {
            try
            {
                File.Delete(LogFilePath);
            }
            catch { enableLogging = false; }
        }

        if (enableLogging && File.Exists(LogFilePath_forStats))
        {
            try
            {
                File.Delete(LogFilePath_forStats);
            }
            catch {  }
        }

		Debug.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;


            if (string.IsNullOrEmpty(mapName))
            {
                //mapName = Map_finland;
                //mapName = Map_germany;
                mapName = Map_usa;
                //mapName = Map_france;
            }
            //new Bot(reader, writer, new Join(botName, botKey));
            //new Bot(reader, writer, new JoinCustom(botName, botKey, Map_finland, 1));
            //new Bot(reader, writer, new JoinCustom(botName, botKey, Map_germany, 1));
            //new Bot(reader, writer, new JoinCustom(botName, botKey, Map_usa, 1));
            //new Bot(reader, writer, new JoinCustom(botName, botKey, Map_france, 1));

            if (CI)
            {
                new Bot(reader, writer, new Join(botName, botKey));
            }
            else
            {
                new Bot(reader, writer, new JoinCustom(botName, botKey, mapName, 1));
            }
            
		}


        Debug.WriteLine("Deaccelaration Values");
        foreach (var val in deAccelarationValues)
        {
            Debug.WriteLine(string.Format("{0}\t{1}",val.Key, val.Value));
        }

        Debug.WriteLine("***************   End   **********************");
	}


	Bot(StreamReader reader, StreamWriter writer, SendMsg join) {
		this.writer = writer;
		string line;

		send(join);

		while((line = reader.ReadLine()) != null) {

            LogMessage(true, line.ToString());

			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
            
            CurrentGameTick = msg.gameTick;
            string formattedData = string.Empty;
			switch(msg.msgType) {
				case "carPositions":
                    CarDataRetrive(msg.data.ToString());
					//send(new Throttle(1));
					break;
				case "join":
					Debug.WriteLine("Joined");
					send(new Ping());
					break;
				case "gameInit":
					Debug.WriteLine("Race init");
                    GameDataInit(msg.data.ToString());
					send(new Throttle(1));
					break;
				case "gameEnd":
					Debug.WriteLine("Race ended");
					send(new Ping());
					break;
				case "gameStart":
					Debug.WriteLine("Race starts");
					send(new Ping());
					break;
                case "crash":
                    Debug.WriteLine("Crash");
                    LogForExcel("Crash");
                    send(new Ping());
                    break;
                case "spawn":
                    Debug.WriteLine(msg.msgType);
                    send(new Throttle(1));
                    break;
                case "yourCar":
                    Debug.WriteLine(msg.msgType);
                    formattedData = msg.data.ToString().Replace("\r\n", "").Replace(" ", "");
                    IdData temp = (IdData)JsonConvert.DeserializeObject<IdData>(formattedData);
                    MyCarName = temp.name;
                    break;
                case "turboAvailable":
                    Debug.WriteLine(msg.msgType);
                     formattedData = msg.data.ToString().Replace("\r\n", "").Replace(" ", "");
                     turboInfo = (TurboInfo)JsonConvert.DeserializeObject<TurboInfo>(formattedData);
                     turboInfo.gameTick = CurrentGameTick;
                     break;
                default:
                    Debug.WriteLine("Unknown messageType ------------- " + msg.msgType);
                    Debug.WriteLine("Data ------------- " + msg.data);
					send(new Ping());
					break;
			}
		}
	}




    private void CarDataRetrive(string data)
    {
        //DateTime currentDateTime = DateTime.Now;

        string formattedData = data.Replace("\r\n", "").Replace(" ", "");
        var tempcarPositions = (CarPosition[])JsonConvert.DeserializeObject<CarPosition[]>(formattedData);
        carPositions = tempcarPositions.ToList();

        #region Get Info

        //get myCar
        myCar = GetMyCar();

        //get speed
        float speed = GetSpeed(myCar.piecePosition.pieceIndex, (float)myCar.piecePosition.inPieceDistance);

        if (speed > MaxSpeed)
            MaxSpeed = speed;

        float averagedSpeed = (previousSpeeds[0] + previousSpeeds[1] + previousSpeeds[2] + speed) / 4.0f;

        //var accelaration = (speed - PreviousSpeed) / ((currentDateTime - PreviousTimeStamp).TotalMilliseconds);
        var accelaration = (speed - PreviousSpeed);


        bool isCarInCurve = gameData.race.track.pieces[myCar.piecePosition.pieceIndex].angle != 0;

        var distBeforeNextCurve = GetDistanceBeforeNextCurve(myCar.piecePosition.pieceIndex, (float)myCar.piecePosition.inPieceDistance);

        int nextCurveIndex = GetNextCurvePieceIndex(myCar.piecePosition.pieceIndex);

        bool turboAvailable = false;
        if (turboInfo != null && (turboInfo.gameTick + turboInfo.turboDurationTicks) > CurrentGameTick)
        {
            turboAvailable = true;
        }

        int nextSwitchPieceIndex = GetNextSwitchPieceIndex();
        bool doLaneSwitch = false;
        string laneSwitchDirection = string.Empty;

        if (throttleValue == 0)
        {
            //float deAccelaration = (float)((speed - PreviousSpeed)/(currentDateTime - PreviousTimeStamp).TotalMilliseconds);
            float deAccelaration = (float)(speed - PreviousSpeed);
            float vel = (float)Math.Round(speed, 2);

            if (deAccelarationValues.ContainsKey(vel))
            {
                float accel = deAccelarationValues[vel];
                float newAccel = (accel + deAccelaration) / 2;
                deAccelarationValues[vel] = newAccel;
            }
            else
            {
                deAccelarationValues[vel] = deAccelaration;
            }
        }

        bool allowSwitch = true;

        #endregion


        if (turboAvailable)
        {
            //Need to calculate when I need to use it
            //1. There is a stretch of straight pieces
            //2. There is enough pieces to sustain slowdown for the next turn

            //Do nothing  
        }

        bool innerLane = false;

        if(myCar.piecePosition.lane.endLaneIndex <= gameData.race.track.lanes.Length/2)
        {
            //car is on left side lane
            if(gameData.race.track.pieces[GetNextCurvePieceIndex(myCar.piecePosition.pieceIndex)].angle> 0)
            {
                innerLane = false;
            }
            else 
            {
                innerLane = true;
            }
        }
        else 
        {
            //car is on right side lane
             if(gameData.race.track.pieces[GetNextCurvePieceIndex(myCar.piecePosition.pieceIndex)].angle> 0)
            {
                innerLane = true;
            }
            else 
            {
                innerLane = false;
            }
        }


        float speedThreshold = GetSpeedBasedOnRadius((float)gameData.race.track.pieces[nextCurveIndex].radius, innerLane);





        #region Car control logic - Gradual slowdown and increase 9:52 lap time -- For first lap

        if (false)
        {

            if (distBeforeNextCurve < 200 || isCarInCurve)
            {
                SlowDown = true;
            }
            else
            {
                SlowDown = false;
            }

            if (SlowDown && speed > speedThreshold)
            {
                throttleValue = throttleValue * .8f;
                if (throttleValue < 0)
                    throttleValue = 0;
            }
            else if (SlowDown && speed < speedThreshold)
            {
                throttleValue = throttleValue * 1.2f;
                if (throttleValue > 1)
                    throttleValue = 1;
            }


            if (isCarInCurve)
            {
                if (Math.Abs(myCar.angle) > 30)
                {
                    SlowDown = true;
                    throttleValue = 0;
                }
                else if (Math.Abs(myCar.angle) > 20 && SlowDown)
                {
                    throttleValue = 0;
                }
                else if (Math.Abs(myCar.angle) > 10 && SlowDown)
                {
                    throttleValue = .5f;
                }
                else
                {
                    throttleValue = .8f;
                }
            }
            else if (!SlowDown)
            {
                throttleValue = 1;
            }
        }

        #endregion




        #region Car control logic - slow down based on sharpness and distance from turn

        if (true)
        {

            if (myCar.piecePosition.pieceIndex + 1 == nextSwitchPieceIndex)
            {
                bool skipLaneChange = false;

                //if (isCarInCurve  || (GetContinuousCurveAngle(nextCurveIndex) < 91 )
                if(isCarInCurve)
                {
                    skipLaneChange = true;
                }


                if (!skipLaneChange)
                {
                    //check if we are in the inner lane for a subsequent turn. We need to move to the outer lane to handle the turns - for faster turns
                    if (gameData.race.track.pieces[nextCurveIndex].angle > 0)
                    {
                        //We need to move right most
                        if (myCar.piecePosition.lane.endLaneIndex < gameData.race.track.lanes.Length - 1)
                        {
                            laneSwitchDirection = "Right";
                            doLaneSwitch = true;
                        }
                    }
                    else
                    {

                        //We need to move left most
                        if (myCar.piecePosition.lane.endLaneIndex > 0)
                        {
                            laneSwitchDirection = "Left";
                            doLaneSwitch = true;
                        }
                    }
                }
            }


            //based on the sharpness of the curve, slow down sufficiently

            //case1: if the car is on a straight stretch and and there are no turns for the next 300units
            if (!isCarInCurve && distBeforeNextCurve > 300)
            {
                throttleValue = 1;
            }
            //case 2: if the car is on a straight stretch and there is a curve coming up
            else if (!isCarInCurve && distBeforeNextCurve >= 200 && distBeforeNextCurve <= 300)
            {
                if (averagedSpeed > speedThreshold)
                {
                    //how big is the gap in speeds?
                    float speedDiff = averagedSpeed - speedThreshold;

                    if (speedDiff >= 2)
                    {
                        throttleValue = 0;
                    }
                    else if (speedDiff >= 1)
                    {
                        throttleValue = .6f;
                    }
                    else if (speedDiff >= .5)
                    {
                        throttleValue = .8f;
                    }
                }
                else
                {
                    throttleValue = 1;
                }
            }
            //case: if the car is on a straight stretch and there is a curve coming up
            else if (!isCarInCurve && distBeforeNextCurve >= 100 && distBeforeNextCurve <= 200)
            {
                if (averagedSpeed > speedThreshold)
                {
                    //how big is the gap in speeds?
                    float speedDiff = averagedSpeed - speedThreshold;

                    if (speedDiff >= 1)
                    {
                        throttleValue = 0;
                    }
                    else if (speedDiff >= .5)
                    {
                        throttleValue = .4f;
                    }
                }
                else
                {
                    throttleValue = 1;
                }
            }
            //case: if the car is on a straight stretch and there is a curve coming up
            else if (!isCarInCurve && distBeforeNextCurve < 100)
            {
                if (averagedSpeed > speedThreshold)
                {
                    throttleValue = 0;
                }
                else
                {
                    throttleValue = 1;
                }
            }
            //case : If the car is in a curve, and over half the distance and there is a straight path ahead, then accelarate
            else if (isCarInCurve && ((double)myCar.piecePosition.inPieceDistance > GetPieceLength(myCar.piecePosition.pieceIndex) / 2.0) &&
                (double)myCar.angle < CurveCriticalAngle && GetNextPiece(myCar.piecePosition.pieceIndex).angle == 0 && GetNextPiece(myCar.piecePosition.pieceIndex).angle == 0)
            {
                // go zooom...
                throttleValue = 1;
            }
            else if (isCarInCurve)
            {
                if (averagedSpeed > speedThreshold)
                {
                    throttleValue = 0;
                }
                else if ((double)myCar.angle > CurveCriticalAngle)
                {
                    throttleValue = 0;
                }
                else if (averagedSpeed < (speedThreshold - .8))
                {
                    throttleValue = 1f;
                }
                else if (averagedSpeed < (speedThreshold - .5))
                {
                    throttleValue = .6f;
                }
                else
                {
                    throttleValue = .4f;
                }
            }

        }

        #endregion



        #region Overtaking, bumping and blocking logics

        {
            var carsAheadofMe = carPositions.Where(a => a.piecePosition.lap > myCar.piecePosition.lap || (a.piecePosition.lap == myCar.piecePosition.lap && a.piecePosition.pieceIndex >= myCar.piecePosition.pieceIndex) ||
                (a.piecePosition.lap == myCar.piecePosition.lap && a.piecePosition.pieceIndex == myCar.piecePosition.pieceIndex && a.piecePosition.inPieceDistance > myCar.piecePosition.inPieceDistance)).ToList();

            var carsBehindMe = carPositions.Where(a => !carsAheadofMe.Contains(a)).ToList();

            bool isMyPathBlockedByAnotherCar = false;

            //how would I know am blocked by a car
            foreach (var car in carsAheadofMe)
            {
                
            }
            



        }

        #endregion



        //if (myCar.piecePosition.lap == 2)
        //{
        //    Debug.WriteLine("Deaccelaration Values");
        //    foreach (var val in deAccelarationValues)
        //    {
        //        Debug.WriteLine(string.Format("{0}\t{1}", val.Key, val.Value));
        //    }
        //}



        #region Update values

        var logaccelaration = accelaration;// *100000; //for convenience
        var logvelocity = speed;// *1000; //for convenience


        if (!statsTitleSet)
        {
            statsTitleSet = true;
            LogForExcel(
                "PieceIndex",
                "InPieceDistance",
                "GameTick",
                "ThrottleValue",
                "Velocity",
                "Accelaration",
                "CarAngle",
                "lane"
                );
        }
        LogForExcel(
            myCar.piecePosition.pieceIndex.ToString(),
            myCar.piecePosition.inPieceDistance.ToString(),
            CurrentGameTick,
            PreviousThrottleValue,
            logvelocity,
            logaccelaration.ToString(),
            myCar.angle.ToString(),
            myCar.piecePosition.lane.endLaneIndex
            );

        Debug.WriteLine(string.Format("PieceIndex: {0} ||  PiecePos: {1}   ||  Lap: {2}  || Speed: {3} || avgSpeed : {4} || accl: {5}|| angle : {7} || Throttle = {8} || speedThreshold = {9} || lane = {10}",
            carPositions[0].piecePosition.pieceIndex,
            Math.Round(carPositions[0].piecePosition.inPieceDistance, 4),
            carPositions[0].piecePosition.lap,
            Math.Round(logvelocity, 4),
            Math.Round(averagedSpeed, 4),
            Math.Round(logaccelaration, 4),
            string.Empty,
            Math.Round(myCar.angle, 4),
            throttleValue,
            speedThreshold,
            myCar.piecePosition.lane.endLaneIndex
            ));


        //Save values. No calculations using the Previous values after this statements
        //Debug.WriteLine((currentDateTime - PreviousTimeStamp).TotalMilliseconds);
        //PreviousTimeStamp = currentDateTime;


        PreviousPieceIndex = myCar.piecePosition.pieceIndex;
        PreviousPieceDistance = (float)myCar.piecePosition.inPieceDistance;
        PreviousSpeed = speed;

        previousSpeeds[0] = previousSpeeds[1];
        previousSpeeds[1] = previousSpeeds[2];
        previousSpeeds[2] = speed;


        #endregion



        //Send relevant action to server

        if (PreviousThrottleValue != throttleValue || CurrentGameTick < 3)
        {
            send(new Throttle(throttleValue));
        }
        else if (doLaneSwitch && !string.IsNullOrEmpty(laneSwitchDirection))
        {
            send(new SwitchLane(laneSwitchDirection));
        }
        //else if (!isInnerLaneSwitchDone && allowSwitch && ((gameData.race.track.pieces[myCar.piecePosition.pieceIndex].Switch && (float)myCar.piecePosition.inPieceDistance < GetPieceLength(myCar.piecePosition.pieceIndex) - 10) || gameData.race.track.pieces[myCar.piecePosition.pieceIndex + 1].Switch))
        //{
        //    //assuming the lane is always 0 on the top
        //    int laneCount = gameData.race.track.lanes.Length - 1;

        //    Debug.WriteLine("mylane: " + myCar.piecePosition.lane.endLaneIndex);

        //    if (myCar.piecePosition.lane.endLaneIndex < laneCount)
        //    {
        //        send(new SwitchLane("Right"));
        //    }

        //    if (myCar.piecePosition.lane.endLaneIndex == laneCount)
        //    {
        //        //it is already in the right most lane. 
        //        isInnerLaneSwitchDone = true;
        //        send(new Ping());
        //    }
        //}        
        else
        {
            send(new Ping());
        }

        PreviousThrottleValue = throttleValue;
    }

    //float GetDistanceBetweenCars(CarPosition car1, CarPosition car2)
    //{
    //    CarPosition carAhead = null;
    //    CarPosition carBehind = null;

    //    if ( car1.piecePosition.pieceIndex > car2.piecePosition.pieceIndex || 
    //        (car1.piecePosition.pieceIndex == car2.piecePosition.pieceIndex && car1.piecePosition.inPieceDistance > car2.piecePosition.inPieceDistance))
    //    {
    //        carAhead = car1;
    //        carBehind = car2;
    //    }
    //    else
    //    {
    //        carAhead = car2;
    //        carBehind = car1;
    //    }


    //    if (carAhead.piecePosition.lap == carBehind.piecePosition.lap)
    //    {

    //    }

    //}

    private float GetContinuousCurveAngle(int index)
    {
        float angle = 0;

        for (int i = index; i < gameData.race.track.pieces.Length; i++)
        {
            if (gameData.race.track.pieces[i].angle > 0)
            {
                angle += Math.Abs((float)gameData.race.track.pieces[i].angle);
            }
            else
            {
                return angle;
            }
        }

        for (int i = 0; i < index; i++)
        {
            if (gameData.race.track.pieces[i].angle > 0)
            {
                angle += Math.Abs((float)gameData.race.track.pieces[i].angle);
            }
            else
            {
                return angle;
            }
        }
        return angle;
    }

  




    #region Supporting functions


    public float GetSpeedBasedOnRadius_forSafeRoute(float radius)
    {
        float speed = 0;
        if (radius < 30)
        {
            speed = 4f;
        }
        else if (radius >= 30 && radius < 60)
        {
            speed = 5f;
        }
        else if (radius >= 60 && radius < 90)
        {
            speed = 5.5f;
        }
        else if (radius >= 90 && radius < 120)
        {
            speed = 6.5f;
        }
        else if (radius >= 120 && radius < 180)
        {
            speed = 7f;
        }
        else if (radius >= 180 && radius < 250)
        {
            speed = 7f;
        }
        else if (radius >= 250)
        {
            speed = 7.5f;
        }
        return speed;
    }


    public float GetSpeedBasedOnRadius(float radius, bool innerLane)
    {
        float speed = 0;

        if (innerLane)
        {

            if (radius < 30)
            {
                speed = 5.5f;
            }
            else if (radius >= 30 && radius < 60)
            {
                speed = 5.5f;
            }
            else if (radius >= 60 && radius < 90)
            {
                speed = 6.5f;
            }
            else if (radius >= 90 && radius < 120)
            {
                speed = 7f;
            }
            else if (radius >= 120 && radius < 180)
            {
                speed = 7.8f;
            }
            else if (radius >= 180 && radius < 250)
            {
                speed = 9.5f;
            }
            else if (radius >= 250)
            {
                speed = 8.5f;
            }
        }
        else
        {
            if (radius < 30)
            {
                speed = 5f;
            }
            else if (radius >= 30 && radius < 60)
            {
                speed = 5f;
            }
            else if (radius >= 60 && radius < 90)
            {
                speed = 6f;
            }
            else if (radius >= 90 && radius < 120)
            {
                speed = 7f;
            }
            else if (radius >= 120 && radius < 180)
            {
                speed = 7f;
            }
            else if (radius >= 180 && radius < 250)
            {
                speed = 7.5f;
            }
            else if (radius >= 250)
            {
                speed = 7.5f;
            }
        }


        return speed;
    }


    /// <summary>
    /// Get my Car from the rest of the other cars
    /// </summary>
    /// <returns></returns>
    private CarPosition GetMyCar()
    {
        for (int i = 0; i < carPositions.Count; i++)
        {
            if (carPositions[i].id.name == MyCarName)
            {
                return carPositions[i];
            }
        }
        return null;
    }

    private float GetPieceLength(int pieceIndex)
    {
        if (pieceIndex < 0)
            return 0;

        if (gameData.race.track.pieces[pieceIndex].angle != 0)
        {
            //this is a curved distance
            float deltaLane = 0;

            if (myCar != null)
            {
                //get the lane delta
                if (gameData.race.track.pieces[pieceIndex].angle > 0)
                {
                    deltaLane = -1 * (float)gameData.race.track.lanes[myCar.piecePosition.lane.startLaneIndex].distanceFromCenter;
                }
                else
                {
                    deltaLane = (float)gameData.race.track.lanes[myCar.piecePosition.lane.startLaneIndex].distanceFromCenter;
                }
            }

            //calculate 2(pi)r and multiple by (angle/360) 
            var circumference = 2.0 * Math.PI * ((double)gameData.race.track.pieces[pieceIndex].radius + deltaLane);
            return Math.Abs((float)((double)gameData.race.track.pieces[pieceIndex].angle / 360.0 * circumference));
        }
        else
        {
            //straight path
            return (float)gameData.race.track.pieces[pieceIndex].length;
        }
    }

    private float GetSpeed(int currentPieceIndex, float currentPieceDistance)
    {
        float speed = 0;
        float deltaDist = 0;

        if (PreviousPieceIndex == currentPieceIndex)
        {
            deltaDist = currentPieceDistance - PreviousPieceDistance;
        }
        else if (PreviousPieceIndex + 1 == currentPieceIndex)
        {
            deltaDist = GetPieceLength(PreviousPieceIndex) - PreviousPieceDistance + currentPieceDistance;
        }
        else
        {
            deltaDist = GetPieceLength(PreviousPieceIndex) - PreviousPieceDistance;
            for (int i = PreviousPieceIndex + 1; i < currentPieceIndex; i++)
            {
                deltaDist += GetPieceLength(i);
            }
            deltaDist += currentPieceDistance;
        }

        speed = deltaDist;

        return (float)speed;
    }
    
    
    /// Get the distance before the next curve(of any curvature) from the current position
    private float GetDistanceBeforeNextCurve(int currentPieceIndex, float currentPieceDistance)
    {
        float dist = 0;
        bool curveEncountered = false;

        dist = GetPieceLength(currentPieceIndex) - currentPieceDistance;
        for (int i = currentPieceIndex+1; i < gameData.race.track.pieces.Length; i++)
        {
            if (gameData.race.track.pieces[i].angle == 0)
            {
                dist += GetPieceLength(i);               
            }
            else
            { 
                curveEncountered = true;
                break;
            }
        }

        if (!curveEncountered)
        {
            for (int i = 0; i < currentPieceIndex; i++)
            {
                if (gameData.race.track.pieces[i].angle == 0)
                {
                    dist += GetPieceLength(i);
                }
                else
                {
                    curveEncountered = true;
                    break;
                }
            }
        }

        return dist;
    }

    private int GetNextCurvePieceIndex(int currentPieceIndex)
    {
      
        for (int i = currentPieceIndex + 1; i < gameData.race.track.pieces.Length; i++)
        {
            if (gameData.race.track.pieces[i].angle != 0)
            {
                return i;
            }
        }

       for (int i = 0; i < currentPieceIndex; i++)
            {
                if (gameData.race.track.pieces[i].angle != 0)
                {
                    return i;
                }                
            }        

        return -1;
    }

    private PieceData GetNextPiece(int index)
    {
        index = index + 1;

        if (index > gameData.race.track.pieces.Length-1)
            index = index - gameData.race.track.pieces.Length;

        return gameData.race.track.pieces[index];
    }

    private int GetNextSwitchPieceIndex()
    {
        int currentPieceIndex = myCar.piecePosition.pieceIndex;

        for (int i = currentPieceIndex + 1; i < gameData.race.track.pieces.Length; i++)
        {
            if (gameData.race.track.pieces[i].Switch)
            {
                return i;
            }
        }

        for (int i = 0; i < currentPieceIndex; i++)
        {
            if (gameData.race.track.pieces[i].Switch)
            {
                return i;
            }
        }

        return -1;
        

    }
    

    private void send(SendMsg msg)
    {
        LogMessage(false, msg.ToJson().ToString());
        //Debug.WriteLine(">>> " + msg.ToJson());
		writer.WriteLine(msg.ToJson());
	}


    private void GameDataInit(string data)
    {
        //Debug.WriteLine(data);

        string formattedData = data.Replace("\r\n", "").Replace(" ", "");
        //var q1 = "{\"race\": {\"track\": {\"id\": \"keimola\",\"name\": \"Keimola\",\"pieces\": [{\"length\": 100.0},{\"length\": 100.0},{\"length\": 100.0},{\"length\": 100.0,\"switch\": true},{\"radius\": 100,\"angle\": 45.0},{\"radius\": 100,\"angle\": 45.0},{\"radius\": 100,\"angle\": 45.0},{\"radius\": 100,\"angle\": 45.0},{\"radius\": 200,\"angle\": 22.5,\"switch\": true},{\"length\": 100.0},{\"length\": 100.0},{\"radius\": 200,\"angle\": -22.5},{\"length\": 100.0},{\"length\": 100.0,\"switch\": true},{\"radius\": 100,\"angle\": -45.0},{\"radius\": 100,\"angle\": -45.0},{\"radius\": 100,\"angle\": -45.0},{\"radius\": 100,\"angle\": -45.0},{\"length\": 100.0,\"switch\": true},{\"radius\": 100,\"angle\": 45.0},{\"radius\": 100,\"angle\": 45.0},{\"radius\": 100,\"angle\": 45.0},{\"radius\": 100,\"angle\": 45.0},{\"radius\": 200,\"angle\": 22.5},{\"radius\": 200,\"angle\": -22.5},{\"length\": 100.0,\"switch\": true},{\"radius\": 100,\"angle\": 45.0},{\"radius\": 100,\"angle\": 45.0},{\"length\": 62.0},{\"radius\": 100,\"angle\": -45.0,\"switch\": true},{\"radius\": 100,\"angle\": -45.0},{\"radius\": 100,\"angle\": 45.0},{\"radius\": 100,\"angle\": 45.0},{\"radius\": 100,\"angle\": 45.0},{\"radius\": 100,\"angle\": 45.0},{\"length\": 100.0,\"switch\": true},{\"length\": 100.0},{\"length\": 100.0},{\"length\": 100.0},{\"length\": 90.0}],\"lanes\": [{\"distanceFromCenter\": -10,\"index\": 0},{\"distanceFromCenter\": 10,\"index\": 1}],\"startingPoint\": {\"position\": {\"x\": -300.0,\"y\": -44.0},\"angle\": 90.0}},\"cars\": [{\"id\": {\"name\": \"Kronos1\",\"color\": \"red\"},\"dimensions\": {\"length\": 40.0,\"width\": 20.0,\"guideFlagPosition\": 10.0}}],\"raceSession\": {\"laps\": 3,\"maxLapTimeMs\": 60000,\"quickRace\": true}}}";
        gameData = JsonConvert.DeserializeObject<GameData>(formattedData);

        PrintTrackInformation();
    }

    private void PrintTrackInformation()
    {
        Debug.WriteLine(String.Format("Track Name - {0}", gameData.race.track.name));
        Debug.WriteLine(String.Format("Track Piece Count - {0}", gameData.race.track.pieces.Length));
        Debug.WriteLine(String.Format("Track StartPoint - {0}", gameData.race.track.startingPoint));

        Debug.WriteLine(String.Format("...Track Data...."));

        int counter = 0;
        foreach(var piece in gameData.race.track.pieces)
        {
            Debug.WriteLine(String.Format(" {0} - Length- {1} | Angle - {2} | Switch - {3} | Radius - {4} | Eff Length - {5}",
                counter,
                piece.length, 
                piece.angle, 
                piece.Switch,                  
                piece.radius,
                GetPieceLength(counter)
                ));
            counter++;
        }
    }


    /// <summary>
    /// strictly Only for log of incoming and outgoing final json messages
    /// </summary>
    /// <param name="isIncoming"></param>
    /// <param name="message"></param>
    void LogMessage(bool isIncoming, string message)
    {
        if (!enableLogging)
            return;

        string finalMessage = "";

        string time = string.Format("{0}:{1}:{2}", DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond);

        if (isIncoming)
        {
            finalMessage = string.Format("<<    {0} : {1}", time , message);
        }
        else
        {
            finalMessage = string.Format(">>    {0} : {1}", time, message);
        }

        try
        {
            File.AppendAllText(LogFilePath, Environment.NewLine + finalMessage);
        }
        catch {
            enableLogging = false;//to avoid logging attempts in the server automated runs
        }
    }

    /// <summary>
    /// Logger for stats - saves pipe separated values to map values on a graph
    /// </summary>
    /// <param name="logParams"></param>
    void LogForExcel(params object[] logParams)
    {
        if (!enableLogging)
            return;

        string finalMessage = "";

        string time = string.Format("{0}:{1}:{2}:{3}", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond);

        finalMessage = time + "\t";

        foreach (object s in logParams)
        {
            finalMessage += s.ToString() + "\t";
        }


        try
        {
            File.AppendAllText(LogFilePath_forStats, Environment.NewLine + finalMessage);
        }
        catch
        {
            enableLogging = false;//to avoid logging attempts in the server automated runs
        }
    }

    #endregion
}
