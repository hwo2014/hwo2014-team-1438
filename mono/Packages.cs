﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace HelloWorldProj
{



    public class MsgWrapper
    {
        public string msgType;
        public Object data;
        public string gameId;
        public int gameTick;

        public MsgWrapper(string msgType, Object data)
        {
            this.msgType = msgType;
            this.gameTick = Bot.CurrentGameTick;
            this.data = data;
        }
    }

    public abstract class SendMsg
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
        }
        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();
    }

    public class Join : SendMsg
    {
        public string name;
        public string key;
        public string color;

        public Join(string name, string key)
        {
            this.name = name;
            this.key = key;
            this.color = "red";
        }

        protected override string MsgType()
        {
            return "join";
        }
    }

    public class SwitchLane : SendMsg
    {
        public string data;

        public SwitchLane(string data)
        {
            this.data = data;
        }

        protected override string MsgType()
        {
            return "switchLane";
        }

        protected override Object MsgData()
        {
            return data;
        }
    }


    public class Ping : SendMsg
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }

    public class Throttle : SendMsg
    {
        public double value;

        public Throttle(double value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }
    }

    public class GameData
    {
        public RaceData race;
    }

    public class RaceData
    {
        public TrackData track;
        public CarData[] cars;
        public RaceSession raceSession;
    }

    public class TrackData
    {
        public string id;
        public string name;
        public PieceData[] pieces;
        public LaneData[] lanes;
        public StartData startingPoint;
    }

    [Serializable]
    public class PieceData
    {
        public decimal length;
        public decimal angle;
        public decimal radius;
        
        public bool Switch;
        public bool bridge;
    }

    public class LaneData
    {
        public decimal distanceFromCenter;
        public int index;
    }

    public class StartData
    {
        public PositionData position;
        public decimal angle;
    }

    public class PositionData
    {
        public decimal x;
        public decimal y;
    }

    public class CarData
    {
        public IdData id;
        public DimensionData dimensions;
    }

    public class IdData
    {
        public string name;
        public string color;
    }

    public class DimensionData
    {
        public decimal lenght;
        public decimal width;
        public decimal guideFlagPosition;
    }

    public class RaceSession
    {
        public int laps;
        public decimal maxLapTime;
        public bool quickRace;
    }


    public class CarPosWrapper
    {
        public CarPosition[] data;
        public string gameId;
        public long gameTick;
    }


    public class CarPosition
    {
        public IdData id;
        public decimal angle;
        public PiecePosition piecePosition;
    }

    public class PiecePosition
    {
        public int pieceIndex;
        public decimal inPieceDistance;
        public Lane lane;
        public int lap;
    }

    public class Lane
    {
        public int startLaneIndex;
        public int endLaneIndex;
    }

    public class JoinCustom: SendMsg
    {
        public BotInfo botId;
        public string trackName;
        public int carCount;

        public JoinCustom(string carName, string carKey, string trackName, int carCount = 1)
        {
            this.botId = new BotInfo() { key = carKey, name = carName };
            this.trackName = trackName;
            this.carCount = carCount;
        }

        protected override string MsgType()
        {
            return "joinRace";
        }
    }


    public class BotInfo
    {
        public string name;
        public string key;
    }

    public class TurboInfo
    {
        public float turboDurationMilliseconds;
        public int turboDurationTicks;
        public float turboFactor;
        public int gameTick;
    }

    public class Turbo : SendMsg
    {
        private string customMessage = string.Empty;

        public Turbo(string messg)
        {
            this.customMessage = messg;
        }

        protected override Object MsgData()
        {
            return this.customMessage;
        }

        protected override string MsgType()
        {
            return "turbo";
        }
    }
}
